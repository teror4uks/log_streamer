# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function

import io 
import json
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse
from rest_framework import status

from django.test import TestCase, override_settings
from django.conf import settings

from .serializers import LogStreamerSerializer


class LoggerTestCases(APITestCase):

    def setUp(self):
        self.lines = '{"level": "DEBUG", "message": "Blah blah blah"}\n\
                      {"level": "INFO", "message": "Everything is fine!"}\n\
                      {"level": "WARN", "message": "Hmmm, wait..."}\n\
                      {"level": "ERROR", "message": "Holly $@#t!"}\n'
        
        self.log_path_name = '/tmp/temp.log'
        self._create_tem_log_file()
        self.url = None

    def _create_tem_log_file(self):
        with io.open(self.log_path_name, 'w') as f:
            for line in self.lines:
                f.write(line)
    
    def test_get_full_offset(self):
        # Then override the LOGIN_URL setting
        self.url = reverse("logger_v1:logger-list")

        expected_res = {
            "ok": True,
            "count": 4,
            "next_offset": 4,
            "messages": [
                {
                    "message": "Blah blah blah",
                    "level": "DEBUG"
                },
                {
                    "message": "Everything is fine!",
                    "level": "INFO"
                },
                {
                    "message": "Hmmm, wait...",
                    "level": "WARN"
                },
                {
                    "message": "Holly $@#t!",
                    "level": "ERROR"
                }
            ]
        }

        with self.settings(PATH_TO_LOG_FILE=self.log_path_name):
            response = self.client.get(self.url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertEqual(response.data['total_size'], 4)
            self.assertEqual(response.data['next_offset'], 4)
    
    def test_get_limit_offset(self):
        self.url = reverse("logger_v1:logger-list")
        self.url = "?".join([self.url, "offset=0&limit=2"])

        with override_settings(PATH_TO_LOG_FILE=self.log_path_name):
            response = self.client.get(self.url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertEqual(response.data['total_size'], 4)
            self.assertEqual(response.data['next_offset'], 2)


    def test_file_not_found(self):
        self.url = reverse("logger_v1:logger-list")
        with self.settings(PATH_TO_LOG_FILE='not exist path'):
            response = self.client.get(self.url)
            self.assertEqual(response.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR)


