from django.conf.urls import url, include
from rest_framework import routers
from .views import LogStreamerViewSet

app_name = 'logger'

router = routers.DefaultRouter()

router.register('read_log', LogStreamerViewSet, base_name='logger')

urlpatterns = router.urls