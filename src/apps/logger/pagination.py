import io
from django.conf import settings
from collections import OrderedDict
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.response import Response
from rest_framework.utils.urls import remove_query_param, replace_query_param

class CustomPagination(LimitOffsetPagination):
    def get_paginated_response(self, data):
        return Response(OrderedDict([
            ('ok', True),
            ('total_size', self.count),
            ('next_offset', self.custom_get_next_link()),
            # ('previous', self.custom_get_previous_link()),
            ('messages', data)
        ]))
    
    def custom_get_next_link(self):
        if self.offset + self.limit >= self.count:
            return self.count

        offset = self.offset + self.limit
        return offset
    
    def get_count(self, queryset):
        with io.open(settings.PATH_TO_LOG_FILE) as f:
            for i, l in enumerate(f):
                pass
        return i + 1
    
    def paginate_queryset(self, queryset, request, view=None):
        self.count = self.get_count(queryset)
        self.limit = self.get_limit(request)

        if self.limit is None:
            return None

        self.offset = self.get_offset(request)
        self.request = request
        if self.count > self.limit and self.template is not None:
            self.display_page_controls = True

        if self.count == 0 or self.offset > self.count:
            return []
        # return list(queryset[self.offset:self.offset + self.limit])
        # We get in this func not all qs, only his cnunk from .get_queryset()
        # and we can't slice it
        return list(queryset)

