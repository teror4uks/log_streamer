from __future__ import print_function

import json
import io
from datetime import datetime
from itertools import islice
from collections import namedtuple

from django.conf import settings


'''
>>> from src.apps.logger import log_reader
>>> l = log_reader.LogStreamer(log_reader.settings.PATH_TO_LOG_FILE)

'''

LogJsonLine = namedtuple('LogJsonLine', ['level', 'message'])

class LogStreamer(object):
    def __init__(self, path, limit, offset=None):
        self.file = path
        if offset is not None:
            self.offset = int(offset)
        else:
            self.offset = 0
        self.limit = int(limit) + self.offset
        self.lines = []
    
    @staticmethod
    def is_empty(line):
        return len(line.strip()) == 0

    def read_strings(self):
        with io.open(self.file, 'r') as f:
            for line in islice(f, self.offset, self.limit, 1):
                if not self.is_empty(line):
                    _line = line.strip().rstrip()
                    line_to_dict = json.loads(_line)
                    log_json_line = LogJsonLine(**line_to_dict)
                    self.lines.append(line_to_dict)

    def get_valid_lines(self):
        self.read_strings()
        return self.lines

