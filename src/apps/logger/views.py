# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import io

from rest_framework import viewsets
from rest_framework import mixins
from rest_framework.response import Response
from rest_framework import status
from django.conf import settings

from .log_reader import LogStreamer
from .serializers import LogStreamerSerializer
from .pagination import CustomPagination


class LogStreamerViewSet(mixins.ListModelMixin, viewsets.ViewSet, CustomPagination):
    serializer_class = LogStreamerSerializer
    
    def filter_queryset(self, queryset):
        return queryset

    def get_queryset(self):
        offset = self.request.query_params.get('offset')
        page_size = self.request.query_params.get('limit') or int(settings.REST_FRAMEWORK['PAGE_SIZE'])
        qs = LogStreamer(settings.PATH_TO_LOG_FILE, offset=offset, limit=page_size)
        valid_lines = []
        try:
            valid_lines = qs.get_valid_lines()
        except IOError as why:
            error = dict(ok=False, reason=why.strerror)
            return Response(error, status=status.HTTP_500_INTERNAL_SERVER_ERROR), True

        return valid_lines, False

    def get_serializer(self, instance=None, data=None, many=False, partial=False):
        return LogStreamerSerializer(data=instance, many=many)

    def list(self, request, *args, **kwargs):
        queryset, error = self.filter_queryset(self.get_queryset())
        if error:
            return queryset

        page = self.paginate_queryset(queryset, request)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            if serializer.is_valid():
                return self.get_paginated_response(serializer.data)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        serializer = self.get_serializer(queryset, many=True)
        if serializer.is_valid():
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        
        