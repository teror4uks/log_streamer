from __future__ import print_function, unicode_literals

from rest_framework import serializers
from rest_framework.fields import ( 
    empty,
)

from .log_reader import LogStreamer


class LogStreamerSerializer(serializers.Serializer):
    message = serializers.CharField(max_length=500)
    level = serializers.CharField(max_length=10)

    class Meta:
        fields = (
            'message',
            'level'
        )
