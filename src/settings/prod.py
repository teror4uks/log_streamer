import os
from django.conf import settings

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = ['log-streamer.herokuapp.com']


# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(settings.BASE_DIR, 'db.sqlite3'),
    }
}

PATH_TO_LOG_FILE = os.path.join(settings.BASE_DIR, 'status.log')
STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'
