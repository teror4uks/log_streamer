# log_streamer

### Prerequisites

 - Django, 
 - Django-Rest-Framework, 
 - Gunicorn, 
 - Whitenoise

### Installing

```
virtualenv ~/<venvs folder>/log_streamer
```

```
sourse ~/<venvs folder>/log_streamer/bin/activate
```

```
pip install -r requarements.txt
```

```
python manage.py collectstatic
```

```
python manage.py migrate
```

```
pytnon manage.py runserver
```

For start with debug mode create dev.py in settings folder like `prod.py`, but enable `DEBUG` to `True` and change `ALLOWED_HOSTS`

```
cp src/settings/prod.py src/settings/dev.py
```

```
nano src/settins/dev.py 
```

```
DEBUG = True
ALLOWED_HOSTS = ['*']
```

Save and exit

Endpoint with read_log here /api/v1/read_log/, you can get first ten lines without parametrs or put ?offset=0, 
if you want change limit for query add to offset parametr limit=[0-9]+ for example

```
/api/v1/read_log/?offset=100&limit=30
```
get 30 lines with offset 100, first line will be 101

## Running the tests
 
```
python manage.py test
```

## Deployment
Test app deployed to [heroku](https://heroku.com) located [here](https://log-streamer.herokuapp.com)
